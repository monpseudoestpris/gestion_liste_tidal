
import os
import datetime
import shutil
DIR="/home/chabert/Tidalrip/Album/"
DIROUT="/home/chabert/Tidalrip/toMove/"

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


def getDirList():
    list_subfolders_with_paths = [f.path for f in os.scandir(DIR) if f.is_dir()]
    return(list_subfolders_with_paths)
def main():
    try:
        os.mkdir(DIROUT)
    except:
        pass
    l=getDirList()
    for d in l:
        first_letter=d.split("/")[-1][0]
        try:
            source=d
            name=source.split("/")[-1]
            destination=DIROUT+first_letter+"/"+name+"/"
            os.mkdir(destination)
        except:
            pass
        

        print("copy from",source,"to",destination)
        copytree(source, destination)  
            
        


if __name__ == "__main__":
    # execute only if run as a script
    main()
