
import os
import datetime
import shutil
DIR="/media/chabert/Samsung_T5/TIDAL/Album/"
DIROUT="/media/chabert/Samsung_T5/TIDAL/TidalRips/"



def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        try:
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
                print("copied",s,d)
            else:
                shutil.copy2(s, d)
                print("copied",s,d)
        except:
            pass


def getDirList():
    list_subfolders_with_paths = [f.path for f in os.scandir(DIR) if f.is_dir()]
    return(list_subfolders_with_paths)
def main():
    try:
        os.mkdir(DIROUT)
    except:
        pass
    l=getDirList()
    for d in l:
        first_letter=d.split("/")[-1][0]
        try:
            source=d
            name=source.split("/")[-1]
            destination=DIROUT+first_letter+"/"+name+"/"
            os.mkdir(destination)
        except:
            pass
        

        print("copy from",source,"to",destination)
        copytree(source, destination)
        shutil.rmtree(source, ignore_errors=True)
        print("removed",source)
            
        


if __name__ == "__main__":
    # execute only if run as a script
    main()
    print("faire le synchro de /media/chabert/TOSHIBA EXT/TIDAL/TidalRips vers donnees2 du NAS")
