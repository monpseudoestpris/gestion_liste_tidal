
import os
import datetime
def create_liste_done_artists():
    if os.path.exists('liste_deja_faits_artists.txt'):
        with open('liste_deja_faits_artists.txt','r') as fread:
            codes_known_artists=[]
            try:
                lines=fread.readlines()[1:]
                for l in lines:
                    lint=(int)(l.split(' ')[0])
                    codes_known_artists.append(lint)
            except:
                pass
        return(codes_known_artists)
    else:
        with open('liste_deja_faits_artists.txt','w') as fwrite:
            fwrite.write('[artist]\n')
            return([])

def create_liste_done_albums():
    if os.path.exists('liste_deja_faits_albums.txt'):
        with open('liste_deja_faits_albums.txt','r') as fread:
            codes_known_albums=[]
            try:
                lines=fread.readlines()[1:]
                for l in lines:
                    lint=(int)(l.split(' ')[0])
                    codes_known_albums.append(lint)
            except:
                pass
        return(codes_known_albums)
    else:
        with open('liste_deja_faits_albums.txt','w') as fwrite:
            fwrite.write('[albums]\n')
            return([])


            
def create_new_list():
    dstr=(str)(datetime.datetime.today()).replace(' ','').replace('-','')[:8]
    if os.path.exists('liste'+dstr+'.txt'):
        pass
    else:
        with open('liste'+dstr+'.txt','w') as fwrite:
            fwrite.write('[artist]\n')
    return(dstr)

def continue_list(mot,nomfichier):
    dstr=nomfichier
    with open('liste'+dstr+'.txt','a') as fwrite:
        fwrite.write('['+mot+']\n')
def main():
    codes_known_artists=create_liste_done_artists()
    codes_known_albums=create_liste_done_albums()
    ######################### artists
    nomfichier=create_new_list()
    code_entered=[]
    for i in  range(50):
        code = input("Artist Id ?")
        if code.lower()=='next':
            break

            
        try:
            code_int=(int)(code)
        
            if code_int in codes_known_artists or code_int in code_entered:
                print("artist already known, skipping")
            else:
                dstr=(str)(datetime.datetime.today()).replace(' ','').replace('-','')[:8]
                with open('liste'+dstr+'.txt','a') as fwrite:
                    fwrite.write((str)(code_int)+'\n')
                with open('liste_deja_faits_artists.txt','a') as fwrite:
                    fwrite.write((str)(code_int)+'\n')
                code_entered.append(code_int)
        except:
            print("problem with code",code_int)
######################### albums
    continue_list('album',nomfichier)
    code_entered=[]
    for i in  range(50):
        code = input("album Id ?")
        if code.lower()=='next':
            break

            
        try:
            code_int=(int)(code)
        
            if code_int in codes_known_albums or code_int in code_entered:
                print("album already known, skipping")
            else:
                dstr=(str)(datetime.datetime.today()).replace(' ','').replace('-','')[:8]
                with open('liste'+dstr+'.txt','a') as fwrite:
                    fwrite.write((str)(code_int)+'\n')
                with open('liste_deja_faits_albums.txt','a') as fwrite:
                    fwrite.write((str)(code_int)+'\n')
                code_entered.append(code_int)
        except:
            print("problem with code",code_int)




if __name__ == "__main__":
    # execute only if run as a script
    main()
